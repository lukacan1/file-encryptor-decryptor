# Crypto tool

Program funguje ako jednoduchý nástroj na šifrovanie/dešifrovanie , kontrolu checksum a podpisovanie súborov. 
<br><br>

Po spustení sa objaví jednoduché GUI ktoré slúži ako menu , obsahuje niekoľko možností
ako ďalej postupovať. Encryption a decryption sa starajú o šifrovanie resp. dešifrovanie súborov , checksum 
sa stará o počítanie zvolenej hash funkcie nad zvoleným súborom , typický príklad na stránke https://www.kali.org/downloads/
má každý súbor svoju checksum ktorú môžme pomocou tohto programu skontrolovať. Sekcia sign a verify sa stará o 
podpisovanie súborov resp. verifikáciu podpisu, ide však o mierne zjednodušenú verziu, táto časť programu dokáže 
pracovať len s kľúčmi nie priamo s certifikátmi. Kľúče či už na podpisovanie alebo šifrovanie rovnako ako aj inicializačný vektor,
je možné vygenerovať.



### Pozor
Prosim nemente obsah v priečinku tests (mysliac iba šifrové texty a plaintext, pridávať sa smie), plaintext je referenčne predšifrovaný (na dôkaz toho že AES funguje správne ) rovnako checksum je referenčne vypočítaný z každého šifrového textu pomocou https://emn178.github.io/online-tools/md5_checksum.html
 

<br><br>
## Inštalácia dependencies:    
```
cd file-encryptor-decryptor
pip install -r requirements.txt
```
## Spustenie:    
```
cd file-encryptor-decryptor
python main.py
```
## Testy:    
```
cd file-encryptor-decryptor
pytest (alebo) python -m pytest
```

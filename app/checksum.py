import hashlib


# -------------------------------------------------------------------------------------------------------------------
# Class which handles checksums
# -------------------------------------------------------------------------------------------------------------------

class CHECKSUM:
    def __init__(self):
        self.__data_set = False
        self.__reference = None
        self.__data = None
        self.__md5_computed = False
        self.__sha_256_computed = False
        self.__sha_512_computed = False

    # -------------------------------------------------------------------------------------------------------------------
    # This function handles checksum compare , if checksum (hash) is already computed the function just compares
    # if to reference , if not, function calls __compute_hash where hash is computed , subsequently , hash is compared
    # -------------------------------------------------------------------------------------------------------------------
    def compare_hash(self, hash_function):
        if not self.__get_hash_computed(hash_function):
            self.__compute_hash(hash_function)
        if hash_function == "MD5":
            if self.__md5 == self.__reference:
                return True
            return False
        elif hash_function == "SHA-256":
            if self.__sha_256 == self.__reference:
                return True
            return False
        elif hash_function == "SHA-512":
            if self.__sha_512 == self.__reference:
                return True
            return False

    def __compute_hash(self, hash_function):
        if hash_function == "MD5":
            self.__md5 = hashlib.md5(self.__data).hexdigest()
            self.__md5_computed = True
        elif hash_function == "SHA-256":
            self.__sha_256 = hashlib.sha256(self.__data).hexdigest()
            self.__sha_256_computed = True
        elif hash_function == "SHA-512":
            self.__sha_512 = hashlib.sha512(self.__data).hexdigest()
            self.__sha_512_computed = True

    # -------------------------------------------------------------------------------------------------------------------
    # Getters
    # -------------------------------------------------------------------------------------------------------------------
    def __get_hash_computed(self, hash_function):
        if hash_function == "MD5":
            return self.__md5_computed
        elif hash_function == "SHA-256":
            return self.__sha_256_computed
        elif hash_function == "SHA-512":
            return self.__sha_512_computed

    def get_data_set(self):
        return self.__data_set

    def get_hash(self, hash_function):
        if not self.__get_hash_computed(hash_function):
            self.__compute_hash(hash_function)
        if hash_function == "MD5":
            return self.__md5
        elif hash_function == "SHA-256":
            return self.__sha_256
        elif hash_function == "SHA-512":
            return self.__sha_512

    # -------------------------------------------------------------------------------------------------------------------
    # Setters
    # -------------------------------------------------------------------------------------------------------------------
    def set_reference(self, reference):
        self.__reference = reference

    def set_file(self, file):
        self.__data = file.read()
        self.__data_set = True
        self.__set_reset_all_hash()
        file.close()

    def __set_reset_all_hash(self):
        self.__md5_computed = False
        self.__sha_256_computed = False
        self.__sha_512_computed = False

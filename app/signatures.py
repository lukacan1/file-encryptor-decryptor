from OpenSSL import crypto

# -------------------------------------------------------------------------------------------------------------------
# These functions handle signatures, basically it signs files and verifies signatures
# -------------------------------------------------------------------------------------------------------------------
class SIGNATURES:
    def __init__(self):
        self.__private_key = crypto.PKey()
        self.__public_key = None
        self.__data = None
        self.__save_as = None
        self.__signature = None
        self.__data_set = False
        self.__private_key_set = False
        self.__public_key_set = False
        self.__save_as_set = False
        self.__signature_set = False

    # -------------------------------------------------------------------------------------------------------------------
    # Sign and verify functions, used only sha512 (sha3-512) here, used hash function is room for improvement
    # -------------------------------------------------------------------------------------------------------------------
    def sign(self):
        try:
            sign = crypto.sign(self.__private_key, self.__data, "sha512")
            self.__write_data(sign, self.__save_as)
            return True
        except crypto.Error:
            return False

    def verify(self):
        try:
            x509 = crypto.X509()
            x509.set_pubkey(self.__public_key)
            crypto.verify(x509, self.__signature, self.__data, "sha512")
            return True
        except crypto.Error:
            return False

    # -------------------------------------------------------------------------------------------------------------------
    # Getters
    # -------------------------------------------------------------------------------------------------------------------
    def get_data_set(self):
        return self.__data_set

    def get_private_key_set(self):
        return self.__private_key_set

    def get_save_as_set(self):
        return self.__save_as_set

    def get_signature_set(self):
        return self.__signature_set

    def get_public_key_set(self):
        return self.__public_key_set

    # -------------------------------------------------------------------------------------------------------------------
    # Setters
    # -------------------------------------------------------------------------------------------------------------------
    def set_file(self, file):
        self.__data = file.read()
        self.__data_set = True
        file.close()

    def set_private_key(self, file):
        try:
            private__key = file.read()
            self.__private_key_set = True
            self.__private_key = crypto.load_privatekey(crypto.FILETYPE_PEM, private__key)
            file.close()
            return True
        except crypto.Error:
            file.close()
            return False

    def set_public_key(self, file):
        try:
            public_key = file.read()
            self.__public_key_set = True
            self.__public_key = crypto.load_publickey(crypto.FILETYPE_PEM, public_key)
            file.close()
            return True
        except crypto.Error:
            file.close()
            return False

    def set_save_as_file(self, file):
        self.__save_as = file
        self.__save_as_set = True

    def set_signature(self, file):
        self.__signature = file.read()
        self.__signature_set = True
        file.close()

    # -------------------------------------------------------------------------------------------------------------------
    # Write data (signature) to selected file
    # -------------------------------------------------------------------------------------------------------------------
    def __write_data(self, data_to_write, where):
        f = open(where, "wb")
        f.write(data_to_write)
        f.close()

    # -------------------------------------------------------------------------------------------------------------------
    # Key pair generator
    # -------------------------------------------------------------------------------------------------------------------
    def key_pair_gen(self, private_key_path, public_key_path, signature_type, key_length):
        if signature_type == "RSA":
            try:
                self.__private_key.generate_key(crypto.TYPE_RSA, key_length)
            except crypto.Error:
                return False
        elif signature_type == "DSA":
            try:
                self.__private_key.generate_key(crypto.TYPE_DSA, key_length)
            except crypto.Error:
                return False
        private_key = crypto.dump_privatekey(crypto.FILETYPE_PEM, self.__private_key)
        public_key = crypto.dump_publickey(crypto.FILETYPE_PEM, self.__private_key)
        self.__write_data(private_key, private_key_path)
        self.__write_data(public_key, public_key_path)
        self.__private_key_set = True
        return True

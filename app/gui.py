from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from PIL import ImageTk
from PIL import Image
from app.checksum import CHECKSUM
from app.crypto_handle import CRYPTOGRAPHY
from app.signatures import SIGNATURES


# -------------------------------------------------------------------------------------------------------------------
# Class handles graphical user interface
# -------------------------------------------------------------------------------------------------------------------


class GUI:
    # -------------------------------------------------------------------------------------------------------------------
    # All buttons , and labels (option menus , text labels) are set here
    # -------------------------------------------------------------------------------------------------------------------
    def __init__(self):
        self.__window = Tk()
        self.__buttons = {
            "encryption": Button(master=self.__window, text="Encryption", height=1, width=13,
                                 command=self.__encryption_window),
            "decryption": Button(master=self.__window, text="Decryption", height=1, width=13,
                                 command=self.__decryption_window),
            "verification": Button(master=self.__window, text="Verify", height=1, width=13,
                                   command=self.__verify_window),
            "signature": Button(master=self.__window, text="Sign", height=1, width=13, command=self.__sign_window),
            "checksum": Button(master=self.__window, text="Checksum", height=1, width=13,
                               command=self.__checksum_window),
            "browse1": Button(master=self.__window, text="Browse", height=1, width=10,
                              command=self.__load_data_crypto),
            "browse2": Button(master=self.__window, text="Browse", height=1, width=10,
                              command=self.__load_data_checksum),
            "browse3": Button(master=self.__window, text="Browse", height=1, width=10,
                              command=self.__load_data_signature),
            "save_as1": Button(master=self.__window, text="Save as", height=1, width=10,
                               command=self.__store_data_crypto),
            "save_as2": Button(master=self.__window, text="Save as", height=1, width=10,
                               command=self.__store_data_signature),
            "load_private_key": Button(master=self.__window, text="Load", height=1, width=10,
                                       command=self.__set_private_key),
            "load_public_key": Button(master=self.__window, text="Load", height=1, width=10,
                                      command=self.__set_public_key),
            "load_signature": Button(master=self.__window, text="Load", height=1, width=10,
                                     command=self.__load_signature),
            "compare_checksum": Button(master=self.__window, text="Compare checksum", height=1, width=15,
                                       command=self.__compare_checksum),
            "show_checksum": Button(master=self.__window, text="Show checksum", height=1, width=15,
                                    command=self.__show_checksum),
            "menu": Button(master=self.__window, text="Menu", height=1, width=15, command=self.__main_window),
            "encrypt": Button(master=self.__window, text="Encrypt", height=1, width=15, command=self.__encryption),
            "decrypt": Button(master=self.__window, text="Decrypt", height=1, width=15, command=self.__decryption),
            "sign": Button(master=self.__window, text="Sign", height=1, width=15, command=self.__sign),
            "verify": Button(master=self.__window, text="Verify", height=1, width=15, command=self.__verify),
            "generate_key": Button(master=self.__window, text="Generate key", height=1, width=15,
                                   command=self.__key_gen),
            "generate_iv": Button(master=self.__window, text="Generate IV", height=1, width=15, command=self.__iv_gen),
            "generate_key_pair": Button(master=self.__window, text="Generate key pair", height=1, width=15,
                                        command=self.__key_pair_gen)}

        self.__ciphers = ["AES-128", "AES-192", "AES-256"]
        self.__cipher_modes = ["ECB", "CBC", "CFB", "OFB"]
        self.__hashes = ["MD5", "SHA-256", "SHA-512"]
        self.__signature_alg = ["RSA", "DSA"]
        self.__key_lengths = [1024, 2048]

        self.__cipher_mode = StringVar()
        self.__cipher_mode.set(self.__cipher_modes[0])

        self.__hash = StringVar()
        self.__hash.set(self.__hashes[0])

        self.__cipher = StringVar()
        self.__cipher.set(self.__ciphers[0])

        self.__signature = StringVar()
        self.__signature.set(self.__signature_alg[0])

        self.__key_length = IntVar()
        self.__key_length.set(self.__key_lengths[1])

        self.__labels = {
            "load_data_entry": Label(master=self.__window, anchor=W, width=70, height=1),
            "store_data_entry": Label(master=self.__window, anchor=W, width=70, height=1),
            "key_file_entry": Label(master=self.__window, anchor=W, width=70, height=1),
            "signature_file_entry": Label(master=self.__window, anchor=W, width=70, height=1),
            "checksum_entry": Text(master=self.__window, width=50, height=1),
            "key_entry": Text(master=self.__window, width=50, height=1),
            "iv_entry": Text(master=self.__window, width=50, height=1),
            "hashes": OptionMenu(self.__window, self.__hash, *self.__hashes),
            "ciphers": OptionMenu(self.__window, self.__cipher, *self.__ciphers),
            "modes": OptionMenu(self.__window, self.__cipher_mode, *self.__cipher_modes),
            "signs": OptionMenu(self.__window, self.__signature, *self.__signature_alg),
            "lengths": OptionMenu(self.__window, self.__key_length, *self.__key_lengths),
            "choose_file": Label(master=self.__window, text="Choose file: "),
            "reference_checksum": Label(master=self.__window, text="Insert reference checksum: "),
            "choose_hash": Label(master=self.__window, text="Choose hash function: "),
            "enter_key": Label(master=self.__window, text="Enter key: "),
            "choose_save_file": Label(master=self.__window, text="Choose where to save: "),
            "enter_iv": Label(master=self.__window, text="Enter IV (it will be used only in CBC,CFB or OFB mode): "),
            "choose_algorithm": Label(master=self.__window,
                                      text="Chosen algorithm and length will be used only if u want to generate key "
                                           "pair"),
            "load_private_key": Label(master=self.__window, text="Load private key"),
            "load_public_key": Label(master=self.__window, text="Load public key"),
            "load_signature": Label(master=self.__window, text="Load file with signature")}
        self.__set_window()
        self.__main_window()

    # -------------------------------------------------------------------------------------------------------------------
    # Set basic window properties (background picture , window size etc.)
    # -------------------------------------------------------------------------------------------------------------------
    def __set_window(self):
        self.__window.title("File encrypt/decrypt/sign/verify/checksum")
        self.__window.geometry("638x361")
        self.__window.resizable(0, 0)
        self.__img = ImageTk.PhotoImage(Image.open("app/background.png"))
        self.__panel = Label(self.__window, image=self.__img)
        self.__panel.place(x=0, y=0, relwidth=1, relheight=1)
        for x, y in self.__labels.items():
            y.lift()
        for x, y in self.__buttons.items():
            y.lift()

    # -------------------------------------------------------------------------------------------------------------------
    # These functions handle what to do and what to show
    # -------------------------------------------------------------------------------------------------------------------
    def __main_window(self):
        self.__set_main_window()

    def __encryption_window(self):
        self.__set_encryption_window()
        self.__cryptography = CRYPTOGRAPHY()

    def __decryption_window(self):
        self.__set_decryption_window()
        self.__cryptography = CRYPTOGRAPHY()

    def __sign_window(self):
        self.__set_sign_window()
        self.__signatures = SIGNATURES()

    def __verify_window(self):
        self.__set_verify_window()
        self.__signatures = SIGNATURES()

    def __checksum_window(self):
        self.__set_checksum_window()
        self.__checksum = CHECKSUM()

    def show(self):
        self.__window.mainloop()

    # -------------------------------------------------------------------------------------------------------------------
    # Load data functions, every part (encryption , checksum , signatures) has different function
    # -------------------------------------------------------------------------------------------------------------------
    def __load_data_checksum(self):

        file_checksum = filedialog.askopenfile(mode="rb", initialdir=".", title="Select file",
                                               filetypes=(("All files", "*.*"),), multiple=0)
        if file_checksum is not None:
            self.__labels["load_data_entry"].config(text=file_checksum.name)
            self.__checksum.set_file(file_checksum)
        else:
            return

    def __load_data_crypto(self):

        data = filedialog.askopenfile(mode="rb", initialdir=".", title="Select file",
                                      filetypes=(("All files", "*.*"),), multiple=0)
        if data is not None:
            self.__labels["load_data_entry"].config(text=data.name)
            self.__cryptography.set_data(data)
        else:
            return

    def __load_data_signature(self):

        data = filedialog.askopenfile(mode="rb", initialdir=".", title="Select file",
                                      filetypes=(("All files", "*.*"),), multiple=0)
        if data is not None:
            self.__labels["load_data_entry"].config(text=data.name)
            self.__signatures.set_file(data)

    def __load_signature(self):
        file = filedialog.askopenfile(mode="rb", initialdir=".", title="Select reference signature",
                                      filetypes=(("All files", "*.*"),), multiple=0)
        if file is not None:
            self.__labels["signature_file_entry"].config(text=file.name)
            self.__signatures.set_signature(file)
        else:
            return

    # -------------------------------------------------------------------------------------------------------------------
    # Set save as files functions , both parts (encryption, signatures) have different function
    # -------------------------------------------------------------------------------------------------------------------
    def __store_data_signature(self):
        save_file = filedialog.asksaveasfile(mode="wb", initialdir=".", title="Select where to store signature",
                                             filetypes=(("All files", "*.*"),))
        if save_file is not None:
            self.__labels["store_data_entry"].config(text=save_file.name)
            self.__signatures.set_save_as_file(save_file.name)
        else:
            return

    def __store_data_crypto(self):
        save_file = filedialog.asksaveasfile(mode="wb", initialdir=".", title="Select file",
                                             filetypes=(("All files", "*.*"),))
        if save_file is not None:
            self.__labels["store_data_entry"].config(text=save_file.name)
            self.__cryptography.set_save_as_file(save_file.name)
        else:
            return

    # -------------------------------------------------------------------------------------------------------------------
    # Load private/public key
    # -------------------------------------------------------------------------------------------------------------------
    def __set_private_key(self):
        key_file = filedialog.askopenfile(mode="rb", initialdir=".", title="Select file",
                                          filetypes=(("All files", "*.pem"),), multiple=0)
        if key_file is not None:
            if not self.__signatures.set_private_key(key_file):
                messagebox.showinfo('Error', "Wrong private key format !")
                return
            else:
                self.__labels["key_file_entry"].config(text=key_file.name)
                return

    def __set_public_key(self):
        key_file = filedialog.askopenfile(mode="rb", initialdir=".", title="Select file",
                                          filetypes=(("All files", "*.pem"),), multiple=0)
        if key_file is not None:
            if not self.__signatures.set_public_key(key_file):
                messagebox.showinfo('Error', "Wrong public key format !")
                return
            else:
                self.__labels["key_file_entry"].config(text=key_file.name)
                return

    # -------------------------------------------------------------------------------------------------------------------
    # Functions which check correctness of inputs and handle communication with classes where individual
    # data is processed
    # -------------------------------------------------------------------------------------------------------------------
    def __compare_checksum(self):
        if not self.__checksum.get_data_set():
            messagebox.showinfo('Error', "Please, choose file !")
            return
        else:
            reference = self.__labels["checksum_entry"].get("1.0", 'end-1c')
            if not len(reference):
                messagebox.showinfo('Error', "Empty reference field, please check it!")
                return
            else:
                self.__checksum.set_reference(reference)
                result = self.__checksum.compare_hash(self.__hash.get())
                if result:
                    messagebox.showinfo('Compare checksum', "Checksum is correct!")
                else:
                    messagebox.showinfo('Compare checksum', "Checksum is incorrect!")

    def __show_checksum(self):
        if not self.__checksum.get_data_set():
            messagebox.showinfo('Error', "Please, choose file !")
        else:
            messagebox.showinfo('checksum', self.__checksum.get_hash(self.__hash.get()))

    def __encryption(self):
        key = self.__labels["key_entry"].get("1.0", 'end-1c')
        chosen_cipher = self.__cipher.get()
        iv = self.__labels["iv_entry"].get("1.0", 'end-1c')
        mode = self.__cipher_mode.get()

        if not self.__check_data_set():
            return
        elif not self.__check_key(key, chosen_cipher):
            return
        elif not self.__check_iv(iv, mode):
            return
        else:
            if self.__cryptography.encryption(chosen_cipher, key, iv, mode):
                messagebox.showinfo('Success', "Ciphertext successfully created")
                return
            else:
                messagebox.showinfo('Failed', "Something went wrong !!")
                return

    def __decryption(self):
        key = self.__labels["key_entry"].get("1.0", 'end-1c')
        chosen_cipher = self.__cipher.get()
        iv = self.__labels["iv_entry"].get("1.0", 'end-1c')
        mode = self.__cipher_mode.get()

        if not self.__check_data_set():
            return
        elif not self.__check_key(key, chosen_cipher):
            return
        elif not self.__check_iv(iv, mode):
            return
        else:
            if self.__cryptography.decryption(chosen_cipher, key, iv, mode):
                messagebox.showinfo('Success', "Plaintext successfully created")
                return
            else:
                messagebox.showinfo('Failed', "Something went wrong !!")
                return

    def __sign(self):
        if not self.__signatures.get_data_set():
            messagebox.showinfo('Error', "Please, choose file !")
            return
        elif not self.__signatures.get_save_as_set():
            messagebox.showinfo('Error', "Please, choose where to store signature !")
            return
        elif not self.__signatures.get_private_key_set():
            messagebox.showinfo('Error', "Please, choose private key or generate key pair !")
            return
        else:
            if self.__signatures.sign():
                messagebox.showinfo('Success', "Signature successfully created")
                return
            else:
                messagebox.showinfo('Failed', "Something went wrong !!")
                return

    def __verify(self):
        if not self.__signatures.get_data_set():
            messagebox.showinfo('Error', "Please, choose file! ")
            return
        elif not self.__signatures.get_signature_set():
            messagebox.showinfo('Error', "Please, choose reference file with signature! ")
            return
        elif not self.__signatures.get_public_key_set():
            messagebox.showinfo('Error', "Please, choose public key! ")
            return
        else:
            if self.__signatures.verify():
                messagebox.showinfo('Success', "Signature successfully checked")
                return
            else:
                messagebox.showinfo('Failed', "Wrong signature, data or key !!")
                return

    # -------------------------------------------------------------------------------------------------------------------
    # Generators
    # -------------------------------------------------------------------------------------------------------------------
    def __key_gen(self):
        key = self.__cryptography.key_gen(self.__cipher.get())
        self.__labels["key_entry"].delete("1.0", 'end')
        self.__labels["key_entry"].insert("1.0", chars=key)

    def __iv_gen(self):
        iv = self.__cryptography.iv_gen(16)
        self.__labels["iv_entry"].delete("1.0", 'end')
        self.__labels["iv_entry"].insert("1.0", chars=iv)

    def __key_pair_gen(self):
        data_private = filedialog.asksaveasfile(mode="wb", initialdir=".", title="Select file for private key",
                                                filetypes=(("Pem files", "*.pem"),))
        if data_private is None:
            return
        data_public = filedialog.asksaveasfile(mode="wb", initialdir=".", title="Select file for public key",
                                               filetypes=(("Pem files", "*.pem"),))
        if data_public is None:
            return
        if self.__signatures.key_pair_gen(data_private.name, data_public.name, self.__signature.get(),
                                          self.__key_length.get()):
            self.__labels["key_file_entry"].config(text=data_private.name)
        else:
            messagebox.showinfo('Error', "Something went wrong! ")

    # -------------------------------------------------------------------------------------------------------------------
    # Setters and windows handlers, only gui settings here. These functions set up location of buttons, labels etc.
    # -------------------------------------------------------------------------------------------------------------------
    def __set_checksum_window(self):
        self.__clear_window(self.__buttons)

        self.__labels["choose_file"].place(x=30, y=10)

        self.__labels["load_data_entry"].place(x=30, y=40)
        self.__labels["load_data_entry"].config(text=" ")

        self.__labels["reference_checksum"].place(x=30, y=75)

        self.__labels["checksum_entry"].place(x=30, y=100)
        self.__labels["checksum_entry"].delete("1.0", 'end')

        self.__labels["choose_hash"].place(x=30, y=130)

        self.__labels["hashes"].place(x=30, y=155)

        self.__buttons["browse2"].place(x=540, y=38)
        self.__buttons["show_checksum"].place(x=30, y=310)
        self.__buttons["compare_checksum"].place(x=150, y=310)
        self.__buttons["menu"].place(x=495, y=310)

    def __set_encryption_window(self):
        self.__set_common_labels_crypto()

        self.__buttons["encrypt"].place(x=30, y=310)
        self.__buttons["generate_key"].place(x=150, y=310)
        self.__buttons["generate_iv"].place(x=270, y=310)
        self.__buttons["menu"].place(x=495, y=310)

    def __set_decryption_window(self):
        self.__set_common_labels_crypto()

        self.__buttons["decrypt"].place(x=30, y=310)
        self.__buttons["menu"].place(x=495, y=310)

    def __set_sign_window(self):
        self.__clear_window(self.__buttons)

        self.__labels["choose_file"].place(x=30, y=15)

        self.__labels["load_data_entry"].place(x=30, y=40)
        self.__labels["load_data_entry"].config(text=" ")

        self.__labels["choose_save_file"].place(x=30, y=75)

        self.__labels["store_data_entry"].place(x=30, y=100)
        self.__labels["store_data_entry"].config(text=" ")

        self.__labels["load_private_key"].place(x=30, y=135)

        self.__labels["key_file_entry"].place(x=30, y=160)
        self.__labels["key_file_entry"].config(text=" ")

        self.__labels["choose_algorithm"].place(x=30, y=195)

        self.__labels["signs"].place(x=30, y=220)
        self.__labels["lengths"].place(x=100, y=220)

        self.__buttons["browse3"].place(x=540, y=38)
        self.__buttons["save_as2"].place(x=540, y=98)
        self.__buttons["load_private_key"].place(x=540, y=158)

        self.__buttons["sign"].place(x=30, y=310)
        self.__buttons["generate_key_pair"].place(x=150, y=310)
        self.__buttons["menu"].place(x=495, y=310)

    def __set_verify_window(self):
        self.__clear_window(self.__buttons)

        self.__labels["choose_file"].place(x=30, y=15)

        self.__labels["load_data_entry"].place(x=30, y=40)
        self.__labels["load_data_entry"].config(text=" ")

        self.__labels["load_signature"].place(x=30, y=75)

        self.__labels["signature_file_entry"].place(x=30, y=100)
        self.__labels["signature_file_entry"].config(text=" ")

        self.__labels["load_public_key"].place(x=30, y=135)

        self.__labels["key_file_entry"].place(x=30, y=160)
        self.__labels["key_file_entry"].config(text=" ")

        self.__buttons["browse3"].place(x=540, y=38)
        self.__buttons["load_signature"].place(x=540, y=98)
        self.__buttons["load_public_key"].place(x=540, y=158)

        self.__buttons["verify"].place(x=30, y=310)
        self.__buttons["menu"].place(x=495, y=310)

    def __set_common_labels_crypto(self):
        self.__clear_window(self.__buttons)

        self.__labels["choose_file"].place(x=30, y=15)

        self.__labels["load_data_entry"].place(x=30, y=40)
        self.__labels["load_data_entry"].config(text=" ")

        self.__labels["choose_save_file"].place(x=30, y=75)

        self.__labels["store_data_entry"].place(x=30, y=100)
        self.__labels["store_data_entry"].config(text=" ")

        self.__labels["enter_key"].place(x=30, y=135)

        self.__labels["key_entry"].place(x=30, y=160)
        self.__labels["key_entry"].delete("1.0", 'end')

        self.__labels["enter_iv"].place(x=30, y=195)

        self.__labels["iv_entry"].place(x=30, y=220)
        self.__labels["iv_entry"].delete("1.0", 'end')

        self.__labels["ciphers"].place(x=30, y=250)
        self.__labels["modes"].place(x=125, y=250)

        self.__buttons["browse1"].place(x=540, y=38)
        self.__buttons["save_as1"].place(x=540, y=98)

    def __set_main_window(self):
        self.__clear_window(self.__buttons)
        self.__clear_window(self.__labels)
        self.__buttons["encryption"].place(x=30, y=310)
        self.__buttons["decryption"].place(x=150, y=310)
        self.__buttons["checksum"].place(x=270, y=310)
        self.__buttons["signature"].place(x=390, y=310)
        self.__buttons["verification"].place(x=510, y=310)

    # -------------------------------------------------------------------------------------------------------------------
    # Auxiliary functions.
    # Some functions in this part could be static, but I like the way of declaring them as private.
    # -------------------------------------------------------------------------------------------------------------------
    def __check_data_set(self):
        if not self.__cryptography.get_data_set():
            messagebox.showinfo('Error', "Please, choose file !")
            return False
        elif not self.__cryptography.get_save_as_set():
            messagebox.showinfo('Error', "Please, choose where to store data !")
            return False
        return True

    def __check_iv(self, iv, mode):
        if mode == "CBC" or mode == "CFB" or mode == "OFB":
            if not len(iv):
                messagebox.showinfo('Error', "Empty IV field, please check it !")
                return False
            elif len(iv) != 16:
                messagebox.showinfo('Error', "Wrong IV length, please check it (should be 16-bytes) !")
                return False
        return True

    def __check_key(self, key, chosen_cipher):
        if not len(key):
            messagebox.showinfo('Error', "Empty key field, please check it!")
            return False
        elif chosen_cipher == "AES-128" and len(key) != 16:
            messagebox.showinfo('Error', "Wrong key length for chosen cipher suit")
            return False
        elif chosen_cipher == "AES-192" and len(key) != 24:
            messagebox.showinfo('Error', "Wrong key length for chosen cipher suit")
            return False
        elif chosen_cipher == "AES-256" and len(key) != 32:
            messagebox.showinfo('Error', "Wrong key length for chosen cipher suit")
            return False
        return True

    def __clear_window(self, clear_what):
        for x, y in clear_what.items():
            y.place_forget()

from app.aes_implementation import AES
import random
import string

# -------------------------------------------------------------------------------------------------------------------
# Class which handles communication between AES class and GUI
# -------------------------------------------------------------------------------------------------------------------
class CRYPTOGRAPHY:
    def __init__(self):
        self.__data_set = False
        self.__data = None
        self.__save_as = None
        self.__save_as_set = False

    # -------------------------------------------------------------------------------------------------------------------
    # Function calls encryption function based on mode
    # -------------------------------------------------------------------------------------------------------------------
    def encryption(self, cipher, key, iv, mode="ECB"):
        if mode == "ECB":
            aes = AES(cipher, key)
            ciphertext = aes.aes_encrypt_ecb(self.__data)
            self.__write_data(ciphertext)
            return True
        elif mode == "CBC":
            aes = AES(cipher, key, mode, iv)
            ciphertext = aes.aes_encrypt_cbc(self.__data)
            self.__write_data(ciphertext)
            return True
        elif mode == "CFB":
            aes = AES(cipher, key, mode, iv)
            ciphertext = aes.aes_encrypt_cfb(self.__data)
            self.__write_data(ciphertext)
            return True
        elif mode == "OFB":
            aes = AES(cipher, key, mode, iv)
            ciphertext = aes.aes_encrypt_ofb(self.__data)
            self.__write_data(ciphertext)
            return True
        else:
            return False

    # -------------------------------------------------------------------------------------------------------------------
    # Function calls decryption function based on mode
    # -------------------------------------------------------------------------------------------------------------------
    def decryption(self, cipher, key, iv, mode="ECB"):
        if mode == "ECB":
            aes = AES(cipher, key)
            plaintext = aes.aes_decrypt_ecb(self.__data)
            self.__write_data(plaintext)
            return True
        elif mode == "CBC":
            aes = AES(cipher, key, mode, iv)
            plaintext = aes.aes_decrypt_cbc(self.__data)
            self.__write_data(plaintext)
            return True
        elif mode == "CFB":
            aes = AES(cipher, key, mode, iv)
            plaintext = aes.aes_decrypt_cfb(self.__data)
            self.__write_data(plaintext)
            return True
        elif mode == "OFB":
            aes = AES(cipher, key, mode, iv)
            plaintext = aes.aes_decrypt_ofb(self.__data)
            self.__write_data(plaintext)
            return True
        else:
            return False

    # -------------------------------------------------------------------------------------------------------------------
    # Simple random key and iv generator
    # -------------------------------------------------------------------------------------------------------------------
    def key_gen(self, cipher_type):
        if cipher_type == "AES-128":
            key_chars = string.ascii_letters + string.digits + string.punctuation
            key = ''.join(random.choice(key_chars) for i in range(16))
            return key
        elif cipher_type == "AES-192":
            key_chars = string.ascii_letters + string.digits + string.punctuation
            key = ''.join(random.choice(key_chars) for i in range(24))
            return key
        elif cipher_type == "AES-256":
            key_chars = string.ascii_letters + string.digits + string.punctuation
            key = ''.join(random.choice(key_chars) for i in range(32))
            return key

    def iv_gen(self, length):
        iv_chars = string.ascii_letters + string.digits + string.punctuation
        iv = ''.join(random.choice(iv_chars) for i in range(length))
        return iv

    # -------------------------------------------------------------------------------------------------------------------
    # Setters
    # -------------------------------------------------------------------------------------------------------------------
    def set_data(self, file):
        self.__data = file.read()
        self.__data = bytearray(self.__data)
        self.__data_set = True
        file.close()

    def set_save_as_file(self, filename):
        self.__save_as = filename
        self.__save_as_set = True

    # -------------------------------------------------------------------------------------------------------------------
    # Getters
    # -------------------------------------------------------------------------------------------------------------------
    def get_data_set(self):
        return self.__data_set

    def get_save_as_set(self):
        return self.__save_as_set

    # -------------------------------------------------------------------------------------------------------------------
    # Write data(ciphertext or decrypted plaintext) to selected file
    # -------------------------------------------------------------------------------------------------------------------
    def __write_data(self, data_to_write):
        f = open(self.__save_as, "wb")
        f.write(data_to_write)
        f.close()

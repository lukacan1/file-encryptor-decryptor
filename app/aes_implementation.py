import copy
import numpy as np

# -------------------------------------------------------------------------------------------------------------------
# SBOX-es are used in sub-bytes and inverted sub-bytes functions, they are typically pre-generated for AES
# -------------------------------------------------------------------------------------------------------------------
sbox = np.uint8([
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5,
    0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0,
    0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc,
    0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
    0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a,
    0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
    0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0,
    0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
    0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b,
    0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85,
    0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5,
    0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17,
    0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88,
    0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c,
    0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9,
    0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6,
    0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e,
    0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94,
    0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68,
    0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16
])
sbox_inv = np.uint8([
    0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
    0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
    0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
    0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
    0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
    0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
    0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
    0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
    0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
    0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
    0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
    0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
    0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
    0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
    0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
    0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D,
])
# -------------------------------------------------------------------------------------------------------------------
# This array is used in key_expansion (expand_key) function
# -------------------------------------------------------------------------------------------------------------------
rcon = np.uint8([0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, ])


# -------------------------------------------------------------------------------------------------------------------
# Main class for AES encryption, class contains round functions and couple of auxiliary functions.
# Some functions in this class could be static, but I like the way of declaring them as private.
# -------------------------------------------------------------------------------------------------------------------

class AES:
    # -------------------------------------------------------------------------------------------------------------------
    # "Constructor" we have to set up here number of rounds and word size (both depend on key length),
    # but word size != block size, block size is same for every key length (32bits) , word size is used only in
    # key expansion function, also initialization vector which is not used in ECB mode.
    # -------------------------------------------------------------------------------------------------------------------
    def __init__(self, aes_type, key, mode="ECB", iv=0):
        if aes_type == "AES-128":
            self.__key = self.__set_key(key)
            self.__n_rounds = 10
            self.__n_words = 4
        elif aes_type == "AES-192":
            self.__key = self.__set_key(key)
            self.__n_rounds = 12
            self.__n_words = 6
        elif aes_type == "AES-256":
            self.__key = self.__set_key(key)
            self.__n_rounds = 14
            self.__n_words = 8
        self.__expanded_key = self.__create_words(self.__key)
        if mode == "CBC" or mode == "CFB" or mode == "OFB":
            self.__iv = self.__set_iv(iv)
            self.__iv = self.__create_words(self.__iv)

    # -------------------------------------------------------------------------------------------------------------------
    # Encrypt mode ECB
    # -------------------------------------------------------------------------------------------------------------------
    def aes_encrypt_ecb(self, data_to_encrypt):
        self.__set_data_encrypt(data_to_encrypt)
        self.__expand_key()
        self.__add_round_key(self.__ciphertext, 0)
        for x in range(1, self.__n_rounds, 1):
            self.__sub_bytes(self.__ciphertext)
            self.__shift_rows(self.__ciphertext)
            self.__mix_columns(self.__ciphertext)
            self.__add_round_key(self.__ciphertext, x)
        self.__sub_bytes(self.__ciphertext)
        self.__shift_rows(self.__ciphertext)
        self.__add_round_key(self.__ciphertext, self.__n_rounds)
        return bytearray(self.__uncreate_words(self.__ciphertext))

    # -------------------------------------------------------------------------------------------------------------------
    # Encrypt mode CBC
    # -------------------------------------------------------------------------------------------------------------------
    def aes_encrypt_cbc(self, data_to_encrypt):
        self.__set_data_encrypt(data_to_encrypt)
        self.__expand_key()
        iv = self.__iv
        for y in range(0, len(self.__ciphertext), 4):
            self.__xor_two_blocks(self.__ciphertext[y:y + 4], iv)
            self.__add_round_key(self.__ciphertext[y:y + 4], 0)
            for x in range(1, self.__n_rounds, 1):
                self.__sub_bytes(self.__ciphertext[y:y + 4])
                self.__shift_rows(self.__ciphertext[y:y + 4])
                self.__mix_columns(self.__ciphertext[y:y + 4])
                self.__add_round_key(self.__ciphertext[y:y + 4], x)
            self.__sub_bytes(self.__ciphertext[y:y + 4])
            self.__shift_rows(self.__ciphertext[y:y + 4])
            self.__add_round_key(self.__ciphertext[y:y + 4], self.__n_rounds)
            iv = self.__ciphertext[y:y + 4]
        return bytearray(self.__uncreate_words(self.__ciphertext))

    # -------------------------------------------------------------------------------------------------------------------
    # Encrypt mode CFB (Simple CFB with 128-bit block size)
    # -------------------------------------------------------------------------------------------------------------------
    def aes_encrypt_cfb(self, data_to_encrypt):
        self.__set_data_encrypt(data_to_encrypt)
        self.__expand_key()

        iv = self.__iv
        for y in range(0, len(self.__ciphertext), 4):
            self.__add_round_key(iv, 0)
            for x in range(1, self.__n_rounds, 1):
                self.__sub_bytes(iv)
                self.__shift_rows(iv)
                self.__mix_columns(iv)
                self.__add_round_key(iv, x)
            self.__sub_bytes(iv)
            self.__shift_rows(iv)
            self.__add_round_key(iv, self.__n_rounds)
            self.__xor_two_blocks(self.__ciphertext[y:y + 4], iv)
            iv = copy.deepcopy(self.__ciphertext[y:y + 4])
        return bytearray(self.__uncreate_words(self.__ciphertext))

    # -------------------------------------------------------------------------------------------------------------------
    # Encrypt mode OFB (Simple OFB with 128-bit block size)
    # -------------------------------------------------------------------------------------------------------------------
    def aes_encrypt_ofb(self, data_to_encrypt):
        self.__set_data_encrypt(data_to_encrypt)
        self.__expand_key()

        iv = copy.deepcopy(self.__iv)
        for y in range(0, len(self.__ciphertext), 4):
            self.__add_round_key(iv, 0)
            for x in range(1, self.__n_rounds, 1):
                self.__sub_bytes(iv)
                self.__shift_rows(iv)
                self.__mix_columns(iv)
                self.__add_round_key(iv, x)
            self.__sub_bytes(iv)
            self.__shift_rows(iv)
            self.__add_round_key(iv, self.__n_rounds)
            self.__xor_two_blocks(self.__ciphertext[y:y + 4], iv)
        return bytearray(self.__uncreate_words(self.__ciphertext))

    # -------------------------------------------------------------------------------------------------------------------
    # Decrypt mode ECB
    # -------------------------------------------------------------------------------------------------------------------
    def aes_decrypt_ecb(self, data_to_decrypt):
        self.__set_data_decrypt(data_to_decrypt)
        self.__expand_key()
        self.__add_round_key(self.__plaintext, self.__n_rounds)
        for y in range((self.__n_rounds - 1), 0, -1):
            self.__shift_rows_inv(self.__plaintext)
            self.__sub_bytes_inv(self.__plaintext)
            self.__add_round_key(self.__plaintext, y)
            self.__mix_columns_inv(self.__plaintext)
        self.__shift_rows_inv(self.__plaintext)
        self.__sub_bytes_inv(self.__plaintext)
        self.__add_round_key(self.__plaintext, 0)
        self.__unpad(self.__plaintext)
        return bytearray(self.__plaintext)

    # -------------------------------------------------------------------------------------------------------------------
    # Decrypt mode CBC
    # -------------------------------------------------------------------------------------------------------------------
    def aes_decrypt_cbc(self, data_to_decrypt):
        self.__set_data_decrypt(data_to_decrypt)
        self.__expand_key()
        iv = copy.deepcopy(self.__iv)
        for y in range(0, len(self.__plaintext), 4):
            next_iv = copy.deepcopy(self.__plaintext[y:y + 4])
            self.__add_round_key(self.__plaintext[y:y + 4], self.__n_rounds)
            for x in range((self.__n_rounds - 1), 0, -1):
                self.__shift_rows_inv(self.__plaintext[y:y + 4])
                self.__sub_bytes_inv(self.__plaintext[y:y + 4])
                self.__add_round_key(self.__plaintext[y:y + 4], x)
                self.__mix_columns_inv(self.__plaintext[y:y + 4])
            self.__shift_rows_inv(self.__plaintext[y:y + 4])
            self.__sub_bytes_inv(self.__plaintext[y:y + 4])
            self.__add_round_key(self.__plaintext[y:y + 4], 0)
            self.__xor_two_blocks(self.__plaintext[y:y + 4], iv)
            iv = copy.deepcopy(next_iv)
        self.__unpad(self.__plaintext)
        return bytearray(self.__plaintext)

    # -------------------------------------------------------------------------------------------------------------------
    # Decrypt mode CFB (Simple CFB with 128-bit block size)
    # -------------------------------------------------------------------------------------------------------------------
    def aes_decrypt_cfb(self, data_to_decrypt):
        self.__set_data_decrypt(data_to_decrypt)
        self.__expand_key()
        iv = copy.deepcopy(self.__iv)
        for y in range(0, len(self.__plaintext), 4):
            self.__add_round_key(iv, 0)
            for x in range(1, self.__n_rounds, 1):
                self.__sub_bytes(iv)
                self.__shift_rows(iv)
                self.__mix_columns(iv)
                self.__add_round_key(iv, x)
            self.__sub_bytes(iv)
            self.__shift_rows(iv)
            self.__add_round_key(iv, self.__n_rounds)
            next_iv = copy.deepcopy(self.__plaintext[y:y + 4])
            self.__xor_two_blocks(self.__plaintext[y:y + 4], iv)
            iv = copy.deepcopy(next_iv)
        self.__unpad(self.__plaintext)
        return bytearray(self.__plaintext)

    # -------------------------------------------------------------------------------------------------------------------
    # Decrypt mode OFB (Simple OFB with 128-bit block size)
    # -------------------------------------------------------------------------------------------------------------------
    def aes_decrypt_ofb(self, data_to_decrypt):
        self.__set_data_decrypt(data_to_decrypt)
        self.__expand_key()
        iv = copy.deepcopy(self.__iv)

        for y in range(0, len(self.__plaintext), 4):
            self.__add_round_key(iv, 0)
            for x in range(1, self.__n_rounds, 1):
                self.__sub_bytes(iv)
                self.__shift_rows(iv)
                self.__mix_columns(iv)
                self.__add_round_key(iv, x)
            self.__sub_bytes(iv)
            self.__shift_rows(iv)
            self.__add_round_key(iv, self.__n_rounds)
            self.__xor_two_blocks(self.__plaintext[y:y + 4], iv)
        self.__unpad(self.__plaintext)
        return bytearray(self.__plaintext)

    # -------------------------------------------------------------------------------------------------------------------
    # AES round functions
    # -------------------------------------------------------------------------------------------------------------------
    def __sub_bytes(self, data):
        for x in data:
            for y in range(4):
                x[y] = sbox[x[y]]

    def __sub_bytes_word(self, word):
        new_word = copy.deepcopy(word)
        for x in range(4):
            new_word[x] = sbox[new_word[x]]
        return new_word

    def __sub_bytes_inv(self, data):
        for x in data:
            for y in range(4):
                x[y] = sbox_inv[x[y]]

    def __shift_rows(self, data):
        for x in range(0, len(data), 4):
            data[x][1], data[x + 1][1], data[x + 2][1], data[x + 3][1] = data[x + 1][1], data[x + 2][1], data[x + 3][1], \
                                                                         data[x][1]
            data[x][2], data[x + 1][2], data[x + 2][2], data[x + 3][2] = data[x + 2][2], data[x + 3][2], data[x][2], \
                                                                         data[x + 1][2]
            data[x][3], data[x + 1][3], data[x + 2][3], data[x + 3][3] = data[x + 3][3], data[x + 0][3], data[x + 1][3], \
                                                                         data[x + 2][3]

    def __shift_rows_inv(self, data):
        for x in range(0, len(data), 4):
            data[x][1], data[x + 1][1], data[x + 2][1], data[x + 3][1] = data[x + 3][1], data[x][1], data[x + 1][1], \
                                                                         data[x + 2][1]
            data[x][2], data[x + 1][2], data[x + 2][2], data[x + 3][2] = data[x + 2][2], data[x + 3][2], data[x][2], \
                                                                         data[x + 1][2]
            data[x][3], data[x + 1][3], data[x + 2][3], data[x + 3][3] = data[x + 1][3], data[x + 2][3], data[x + 3][3], \
                                                                         data[x][3]

    def __mix_columns(self, data):
        for x in range(len(data)):
            b0 = np.uint8(
                (self.__xtime(data[x][0])) ^ (self.__xtime(data[x][1]) ^ data[x][1]) ^ data[x][2] ^ data[x][3])
            b1 = np.uint8(
                (data[x][0]) ^ (self.__xtime(data[x][1])) ^ ((self.__xtime(data[x][2])) ^ data[x][2]) ^ data[x][3])
            b2 = np.uint8(data[x][0] ^ data[x][1] ^ self.__xtime(data[x][2]) ^ (self.__xtime(data[x][3]) ^ data[x][3]))
            b3 = np.uint8((self.__xtime(data[x][0]) ^ data[x][0]) ^ data[x][1] ^ data[x][2] ^ self.__xtime(data[x][3]))
            data[x][0] = b0
            data[x][1] = b1
            data[x][2] = b2
            data[x][3] = b3

    def __mix_columns_inv(self, data):
        for x in range(len(data)):
            b0 = np.uint8((self.__xtime(self.__xtime(self.__xtime(data[x][0]) ^ data[x][0]) ^ data[x][0]))
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][1])) ^ data[x][1]) ^ data[x][1])
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][2]) ^ data[x][2])) ^ data[x][2])
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][3]))) ^ data[x][3]))
            b1 = np.uint8((self.__xtime(self.__xtime(self.__xtime(data[x][0]))) ^ data[x][0])
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][1]) ^ data[x][1]) ^ data[x][1]))
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][2])) ^ data[x][2]) ^ data[x][2])
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][3]) ^ data[x][3])) ^ data[x][3]))
            b2 = np.uint8((self.__xtime(self.__xtime(self.__xtime(data[x][0]) ^ data[x][0])) ^ data[x][0])
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][1]))) ^ data[x][1])
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][2]) ^ data[x][2]) ^ data[x][2]))
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][3])) ^ data[x][3]) ^ data[x][3]))
            b3 = np.uint8((self.__xtime(self.__xtime(self.__xtime(data[x][0])) ^ data[x][0]) ^ data[x][0])
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][1]) ^ data[x][1])) ^ data[x][1])
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][2]))) ^ data[x][2])
                          ^ (self.__xtime(self.__xtime(self.__xtime(data[x][3]) ^ data[x][3]) ^ data[x][3])))
            data[x][0] = b0
            data[x][1] = b1
            data[x][2] = b2
            data[x][3] = b3

    def __add_round_key(self, data, num_round):
        for x in range(len(data)):
            for y in range(4):
                data[x][y] = data[x][y] ^ self.__expanded_key[(x % 4) + (4 * num_round)][y]

    def __expand_key(self):
        for x in range((self.__n_rounds + 1) * 4):
            if x < self.__n_words:
                pass
            elif x >= self.__n_words and x % self.__n_words == 0:
                tmp = self.__rot_word(self.__expanded_key[x - 1])
                tmp = self.__sub_bytes_word(tmp)
                for y in range(4):
                    tmp[y] = tmp[y] ^ self.__expanded_key[x - self.__n_words][y]
                tmp[0] = tmp[0] ^ rcon[x // self.__n_words]
                self.__expanded_key.append(copy.deepcopy(tmp))
            elif x >= self.__n_words > 6 and x % self.__n_words == 4:
                tmp = self.__sub_bytes_word(self.__expanded_key[x - 1])
                for y in range(4):
                    tmp[y] = tmp[y] ^ self.__expanded_key[x - self.__n_words][y]
                self.__expanded_key.append(copy.deepcopy(tmp))
            else:
                for y in range(4):
                    tmp[y] = self.__expanded_key[x - 1][y] ^ self.__expanded_key[x - self.__n_words][y]
                self.__expanded_key.append(copy.deepcopy(tmp))

    # -------------------------------------------------------------------------------------------------------------------
    # Setters
    # -------------------------------------------------------------------------------------------------------------------
    def __set_data_decrypt(self, data_to_decrypt):
        data = [x for x in data_to_decrypt]
        self.__plaintext = self.__create_words(data)

    def __set_data_encrypt(self, data_to_encrypt):
        raw_data = [c for c in data_to_encrypt]
        self.__pad(raw_data)
        self.__ciphertext = self.__create_words(self.__ciphertext)

    def __set_key(self, key):
        return [ord(c) for c in key]

    def __set_iv(self, iv):
        return [ord(c) for c in iv]

    # -------------------------------------------------------------------------------------------------------------------
    # PKCS#7 padding
    # -------------------------------------------------------------------------------------------------------------------
    def __pad(self, data):
        padding_len = 16 - (len(data) % 16)
        padding = ([padding_len] * padding_len)
        self.__ciphertext = data + padding

    def __unpad(self, data):
        new_data = self.__uncreate_words(data)
        padding_len = new_data[len(new_data) - 1]
        self.__plaintext = new_data[:-padding_len]

    # -------------------------------------------------------------------------------------------------------------------
    # Auxiliary functions for AES
    # -------------------------------------------------------------------------------------------------------------------
    def __rot_word(self, data):
        return [data[1], data[2], data[3], data[0]]

    def __xtime(self, data):
        mask = np.uint8(0x80)
        m = np.uint8(0x1b)
        high_bit = np.uint8(data & mask)
        final = np.uint8(data << 1)
        if high_bit:
            final = final ^ m
        return final

    def __xor_two_blocks(self, data1, data2):
        for a, b in zip(data1, data2):
            for y in range(4):
                a[y] = a[y] ^ b[y]

    # -------------------------------------------------------------------------------------------------------------------
    # Functions for list adjustment
    # __create_words    --> creates 2d-array from 1d-array small arrays are 32-words
    # __uncreate_words  --> un-create 2d-array to 1d-array
    # -------------------------------------------------------------------------------------------------------------------
    def __create_words(self, data):
        return [list(data[i:i + 4]) for i in range(0, len(data), 4)]

    def __uncreate_words(self, data):
        new_data = list()
        for x in range(len(data)):
            for y in range(4):
                new_data.append(copy.deepcopy(data[x][y]))
        return new_data

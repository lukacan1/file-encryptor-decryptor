from app.checksum import CHECKSUM
from numpy.testing import assert_equal


# -------------------------------------------------------------------------------------------------------------------
# Reference checksums were calculated by online checksum tools
# -------------------------------------------------------------------------------------------------------------------
def test_checksum_md5_1():
    checksum = CHECKSUM()
    ref_checksum = "a5e0489e9cb86de970c1301e3b29ebd9"
    file = open("tests/aes_128_cbc_ciphertext", "rb")
    checksum.set_reference(ref_checksum)
    checksum.set_file(file)
    result = checksum.compare_hash("MD5")
    assert_equal(result, True)

def test_checksum_md5_2():
    checksum = CHECKSUM()
    ref_checksum = "406c496d8dbd5e7e3536c786a55ccb72"
    file = open("tests/aes_128_cfb_ciphertext", "rb")
    checksum.set_reference(ref_checksum)
    checksum.set_file(file)
    result = checksum.compare_hash("MD5")
    assert_equal(result, True)


def test_checksum_sha_256_1():
    checksum = CHECKSUM()
    ref_checksum = "e15adc3e18c958920433c4c277e82264a7157365ddc5dbcd9587e37e9dfa57e8"
    file = open("tests/aes_128_ofb_ciphertext", "rb")
    checksum.set_reference(ref_checksum)
    checksum.set_file(file)
    result = checksum.compare_hash("SHA-256")
    assert_equal(result, True)

def test_checksum_sha_256_2():
    checksum = CHECKSUM()
    ref_checksum = "f35b7a1f4f2c6aa75c289cce60e560f757979979808d5cdcbceb9293e0357b41"
    file = open("tests/aes_192_cbc_ciphertext", "rb")
    checksum.set_reference(ref_checksum)
    checksum.set_file(file)
    result = checksum.compare_hash("SHA-256")
    assert_equal(result, True)


def test_checksum_sha_512_1():
    checksum = CHECKSUM()
    ref_checksum = "99fa374f5eba0370a5275cf905bc8662bca91c26bffc96b98472fb3b7f2efd236a4c0b3c21de5fcaa3a4003cd08afbfc0cb6d6067f32e98d7259d7e480dcaa5b"
    file = open("tests/aes_256_cbc_ciphertext", "rb")
    checksum.set_reference(ref_checksum)
    checksum.set_file(file)
    result = checksum.compare_hash("SHA-512")
    assert_equal(result, True)

def test_checksum_sha_512_2():
    checksum = CHECKSUM()
    ref_checksum = "d0a4598f1b24049df28318775b9473a47c3a92db92753e034cfc7d8fe9636dfc05e947404e4f402481a8cb3b3aac63437c7d8eb326823a639f4e06da5ad47080"
    file = open("tests/aes_256_ecb_ciphertext", "rb")
    checksum.set_reference(ref_checksum)
    checksum.set_file(file)
    result = checksum.compare_hash("SHA-512")
    assert_equal(result, True)
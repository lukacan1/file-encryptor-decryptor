from app.aes_implementation import AES
from app.crypto_handle import CRYPTOGRAPHY
from numpy.testing import assert_equal


def set_data(file_name):
    file = open(file_name, "rb")
    data = file.read()
    data = bytearray(data)
    file.close()
    return data

# -------------------------------------------------------------------------------------------------------------------
# Reference ciphertexts were calculated by online encryption tools
# -------------------------------------------------------------------------------------------------------------------

def test_crypto_aes_128_cbc():
    data_plaintext = set_data("tests/plaintext.txt")
    data_ciphertext_ref = set_data("tests/aes_128_cbc_ciphertext")
    aes = AES("AES-128", "0123456789abcdef", "CBC", "0123456789abcdef")
    ciphertext = aes.aes_encrypt_cbc(data_plaintext)
    assert_equal(ciphertext, data_ciphertext_ref)


def test_crypto_aes_192_cbc():
    data_plaintext = set_data("tests/plaintext.txt")
    data_ciphertext_ref = set_data("tests/aes_192_cbc_ciphertext")
    aes = AES("AES-192", "0123456789abcdef01234567", "CBC", "0123456789abcdef")
    ciphertext = aes.aes_encrypt_cbc(data_plaintext)
    assert_equal(ciphertext, data_ciphertext_ref)


def test_crypto_aes_256_cbc():
    data_plaintext = set_data("tests/plaintext.txt")
    data_ciphertext_ref = set_data("tests/aes_256_cbc_ciphertext")
    aes = AES("AES-256", "0123456789abcdef0123456701234567", "CBC", "0123456789abcdef")
    ciphertext = aes.aes_encrypt_cbc(data_plaintext)
    assert_equal(ciphertext, data_ciphertext_ref)


def test_crypto_aes_256_ecb():
    data_plaintext = set_data("tests/plaintext.txt")
    data_ciphertext_ref = set_data("tests/aes_256_ecb_ciphertext")
    aes = AES("AES-256", "0123456789abcdef0123456701234567")
    ciphertext = aes.aes_encrypt_ecb(data_plaintext)
    assert_equal(ciphertext, data_ciphertext_ref)


def test_crypto_aes_128_cfb():
    data_plaintext = set_data("tests/plaintext.txt")
    data_ciphertext_ref = set_data("tests/aes_128_cfb_ciphertext")
    aes = AES("AES-128", "0123456789abcdef", "CFB", "0123456789abcdef")
    ciphertext = aes.aes_encrypt_cfb(data_plaintext)
    assert_equal(ciphertext, data_ciphertext_ref)


def test_crypto_aes_128_ofb():
    data_plaintext = set_data("tests/plaintext.txt")
    data_ciphertext_ref = set_data("tests/aes_128_ofb_ciphertext")
    aes = AES("AES-128", "0123456789abcdef", "OFB", "0123456789abcdef")
    ciphertext = aes.aes_encrypt_ofb(data_plaintext)
    assert_equal(ciphertext, data_ciphertext_ref)


def test_random_aes_256_ofb():
    crypto = CRYPTOGRAPHY()
    key = crypto.key_gen("AES-256")
    iv = crypto.iv_gen(16)
    data_plaintext_ref = set_data("tests/plaintext.txt")
    aes = AES("AES-256", key, "OFB", iv)
    ciphertext = aes.aes_encrypt_ofb(data_plaintext_ref)
    plaintext = aes.aes_decrypt_ofb(ciphertext)
    assert_equal(data_plaintext_ref, plaintext)


def test_random_aes_256_cbc():
    crypto = CRYPTOGRAPHY()
    key = crypto.key_gen("AES-256")
    iv = crypto.iv_gen(16)
    data_plaintext_ref = set_data("tests/plaintext.txt")
    aes = AES("AES-256", key, "CBC", iv)
    ciphertext = aes.aes_encrypt_cbc(data_plaintext_ref)
    plaintext = aes.aes_decrypt_cbc(ciphertext)
    assert_equal(data_plaintext_ref, plaintext)
